package general

import (
	"encoding/json"
	"log"
)

//HTTPResponse for all the requests
type HTTPResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

// Check function checks whether an error is nil or not and prints the message if its not nil
func Check(e error, f bool, msg ...string) {
	if e != nil && f == true {
		log.Fatalf("FATAL: %s %v", msg, e)
	}
	if e != nil {
		log.Println("ERROR:", msg, e)
	}
	return
}

// PrepareHTTPResponse prepares an HTTP Response
func PrepareHTTPResponse(status int, message string, response *HTTPResponse) ([]byte, error) {
	response.Message = message
	response.Status = status
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		log.Println("ALERT: Error marshalling json in prepareHTTPResponse()", err)
		return nil, err
	}
	return jsonResponse, nil
}
